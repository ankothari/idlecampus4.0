class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :group_id
      
      t.integer :targetable_id
      t.string :targetable_type
      t.timestamps
    end
    
    add_index :posts,:group_id
    add_index :posts,[:targetable_id,:targetable_type]
  end
end
