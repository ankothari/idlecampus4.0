require 'spec_helper'

describe Group do
  it { should have_many(:posts) }
  it {should have_many :teachers }
  it {should have_many :subjects} 
  it {should have_many :rooms}
  it {should respond_to(:get_field_entries)}
  context "#create_post" do
    it "increase the Activity count" do
      group = FactoryGirl.create(:group)
      note = group.notes.new
      note.stub(:send_push).and_return(true)
      expect{group.create_post(note)}.to change{Post.count}.by(1)
    end
  end

   context "#get_field_entries" do
    it "returns the field entries" do
      group = FactoryGirl.create(:group)
      timetable = create(:timetable,group: group)
      field_entries = group.get_field_entries
      field_entries.should eq([{:name=>"room", :values=>group.rooms.pluck(:name)}, {:name=>"teacher", :values=>group.teachers.pluck(:name)}, {:name=>"subject", :values=>group.subjects.pluck(:name)}])
    end
  end
end
