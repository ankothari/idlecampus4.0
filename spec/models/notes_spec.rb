require 'spec_helper'

describe Note do
	it { should have_one(:post) }
  it { should respond_to(:file) }
  it { should belong_to(:group) }
  it {should respond_to(:send_push)}
end