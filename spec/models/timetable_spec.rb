require 'spec_helper'

describe Timetable do
  let(:group) { create(:group_with_rooms) }
  let(:result) { timetable = {"group_code"=>group.group_code, "entries"=>[[[{"from_hours"=>"09", "from_minutes"=>"10", "to_minutes"=>"20", "to_hours"=>"11", "batch"=>"C1", "weekday"=>"Monday", "$$hashKey"=>"06B", "teacher"=>"teacher1", "subject"=>"subject1", "room"=>"room1"}, {"from_hours"=>"09", "from_minutes"=>"10", "to_minutes"=>"20", "to_hours"=>"11", "batch"=>"C2", "weekday"=>"Monday", "$$hashKey"=>"06D", "teacher"=>"teacher1", "subject"=>"subject1", "room"=>"room1"}, {"from_hours"=>"09", "from_minutes"=>"10", "to_minutes"=>"20", "to_hours"=>"11", "batch"=>"C3", "weekday"=>"Monday", "$$hashKey"=>"06F", "teacher"=>"teacher1", "subject"=>"subject1", "room"=>"room1"}], [{"from_hours"=>"09", "from_minutes"=>"10", "to_minutes"=>"20", "to_hours"=>"11", "batch"=>"C1", "weekday"=>"Tuesday", "$$hashKey"=>"081", "teacher"=>"teacher1", "subject"=>"subject1", "room"=>"room1"}, {"from_hours"=>"09", "from_minutes"=>"10", "to_minutes"=>"20", "to_hours"=>"11", "batch"=>"C2", "weekday"=>"Tuesday", "$$hashKey"=>"083", "teacher"=>"teacher1", "subject"=>"subject1", "room"=>"room1"}, {"from_hours"=>"09", "from_minutes"=>"10", "to_minutes"=>"20", "to_hours"=>"11", "batch"=>"C3", "weekday"=>"Tuesday", "$$hashKey"=>"085", "teacher"=>"teacher1", "subject"=>"subject1", "room"=>"room1"}], [{"from_hours"=>"09", "from_minutes"=>"10", "to_minutes"=>"20", "to_hours"=>"11", "weekday"=>"Wednesday", "$$hashKey"=>"055", "teacher"=>"teacher3", "subject"=>"subject3", "room"=>"room3"}], [{"from_hours"=>"09", "from_minutes"=>"10", "to_minutes"=>"20", "to_hours"=>"11", "weekday"=>"Thursday", "$$hashKey"=>"05P", "teacher"=>"teacher4", "subject"=>"subject4", "room"=>"room4"}]]], "members"=>nil}
      }
  it {should belong_to(:group)}
	it {should have_many :timetable_entries}
  it {should have_many :weekdays}
  it {should respond_to(:get_field_entries)}
  it { should respond_to(:get_entries_array)}
  it {should respond_to(:get_entries)}
  it {should respond_to(:build_timetable_entries)}
  
  context "#get_field_entries" do
  	it "returns the field entries" do
  		timetable = create(:timetable,group: group)
  		field_entries = timetable.get_field_entries
      field_entries.should eq([{:name=>"room", :values=>group.rooms.pluck(:name)}, {:name=>"teacher", :values=>group.teachers.pluck(:name)}, {:name=>"subject", :values=>group.subjects.pluck(:name)}])
  	end
  end
  
  context "#build_timetable_entries" do
  	it "creates the entries for the timetable" do
  		PygmentsWorker.stub(:perform_async).and_return(true)
  		timetable = create(:timetable,group: group)
  		timetable.build_timetable_entries(result['entries'])
  		timetable_entries = timetable.timetable_entries
  		timetable_entries.should_not be_empty
  	end
  end



   context "#get_entries_array " do
  	it "returns the entries in an array" do
  		PygmentsWorker.stub(:perform_async).and_return(true)
  		timetable = create(:timetable,group: group)
  		timetable.build_timetable_entries(result['entries'])
  		timetable_entries = timetable.timetable_entries
  	  entries_array = timetable.get_entries_array
      expect(entries_array).to eq([[[{"to_hours"=>"11", "to_minutes"=>"20", "from_minutes"=>"10", "from_hours"=>"9", "teacher"=>"teacher1", "subject"=>"subject1", "room"=>"room1", "batch"=>"C1", "weekday"=>"Monday"}, {"to_hours"=>"11", "to_minutes"=>"20", "from_minutes"=>"10", "from_hours"=>"9", "teacher"=>"teacher1", "subject"=>"subject1", "room"=>"room1", "batch"=>"C2", "weekday"=>"Monday"}, {"to_hours"=>"11", "to_minutes"=>"20", "from_minutes"=>"10", "from_hours"=>"9", "teacher"=>"teacher1", "subject"=>"subject1", "room"=>"room1", "batch"=>"C3", "weekday"=>"Monday"}], [{"to_hours"=>"11", "to_minutes"=>"20", "from_minutes"=>"10", "from_hours"=>"9", "teacher"=>"teacher1", "subject"=>"subject1", "room"=>"room1", "batch"=>"C1", "weekday"=>"Tuesday"}, {"to_hours"=>"11", "to_minutes"=>"20", "from_minutes"=>"10", "from_hours"=>"9", "teacher"=>"teacher1", "subject"=>"subject1", "room"=>"room1", "batch"=>"C2", "weekday"=>"Tuesday"}, {"to_hours"=>"11", "to_minutes"=>"20", "from_minutes"=>"10", "from_hours"=>"9", "teacher"=>"teacher1", "subject"=>"subject1", "room"=>"room1", "batch"=>"C3", "weekday"=>"Tuesday"}], [{"to_hours"=>"11", "to_minutes"=>"20", "from_minutes"=>"10", "from_hours"=>"9", "teacher"=>"teacher3", "subject"=>"subject3", "room"=>"room3", "batch"=>nil, "weekday"=>"Wednesday"}], [{"to_hours"=>"11", "to_minutes"=>"20", "from_minutes"=>"10", "from_hours"=>"9", "teacher"=>"teacher4", "subject"=>"subject4", "room"=>"room4", "batch"=>nil, "weekday"=>"Thursday"}]]]) 	
    end
  end
end