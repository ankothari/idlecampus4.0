require 'spec_helper'

describe TimetableEntry do
	it {should belong_to :timetable}
  it {should belong_to :weekday}
  it {should belong_to :subject}
  it {should belong_to :teacher}
  it {should belong_to :room}
  it {should belong_to :class_timing}
  it {should belong_to :small_group}
  it {should respond_to(:create_entry)}
end