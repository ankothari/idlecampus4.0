require 'spec_helper' 

describe SessionsController do
	context "#create" do
		it "logs in the user" do
			user = create(:user)
			RubyBOSH.stub(:register).and_return(true)
		  RubyBOSH.stub(:initialize_session).and_return([1,1,1])
			resp = post :create,session: {email: user.email, password: user.password}
			response.code.should == "302"
      response.should redirect_to(home_path)
    end

    it "does not log in the user" do
			user = build(:user)
			RubyBOSH.stub(:register).and_return(true)
		  RubyBOSH.stub(:initialize_session).and_return([1,1,1])
			resp = post :create,session: {email: user.email, password: user.password }
			response.code.should == "200"
      response.should render_template('new')
    end

	end
end