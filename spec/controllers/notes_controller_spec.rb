require 'spec_helper'
require 'carrierwave/test/matchers'
describe NotesController do
  include CarrierWave::Test::Matchers

  before do
    FileUploader.enable_processing = true
    @uploader = FileUploader.new(@user, :avatar)
    # @uploader.store!(File.open('/Users/apple/Desktop/facebook.png'))
  end

  after do
    FileUploader.enable_processing = false
    @uploader.remove!
  end
  context "#create" do
  	it "creates a note for the group if the file is present" do
	    group = create(:group)
	    PygmentsWorker.stub(:perform_async).and_return(true)
	  	 
	    expect{
	       post :create, :format => 'js',group: group.group_code,  note_text:"I am uploading a new file", members:["a@a.com","b@b.com"],note: {file:@uploader }
	   
	      }.to change(Note, :count).by(1)

	    expect(group.posts.count).to eq(1)
	    expect(group.posts.first.targetable_type).to eq("Note")
	    expect(group.posts.first.targetable_id).to eq(1)
    end

    it "creates a note for the group if the file is not present" do
	    group = create(:group)
	    PygmentsWorker.stub(:perform_async).and_return(true)
	  	 
	    expect{
	       post :create, :format => 'js',group: group.group_code, note_text:"I am uploading a new file",members:["a@a.com","b@b.com"],note: {file: nil}
	   
	      }.to change(Note, :count).by(1)

	    expect(group.posts.count).to be(1)
	    expect(group.posts.first.targetable_type).to eq("Note")
	    expect(group.posts.first.targetable_id).to eq(1)
    end
  end

  context "#index" do
  	it "returns the notes as a json" do
  		PygmentsWorker.stub(:perform_async).and_return(true)
  		group = create(:group)
  		note1 = create(:note,group: group)
  		note1.file = @uploader
  		note1.save
  		note2 = create(:note,group: group)
  		note2.file = @uploader
  		note2.save
  		get :index,group_id: group.group_code
  		notes = ActiveSupport::JSON.decode(response.body)
  		notes.should == {"files"=>[{"id"=>1, "url"=>nil, "message"=>"This is a new note."}, {"id"=>2, "url"=>nil, "message"=>"This is a new note."}]}
    end
  end
end
