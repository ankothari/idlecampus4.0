require 'spec_helper'

describe HomesController do
  context "#show" do
    it "returns the posts in a reverse order for a teacher" do
      get :show
      user = create(:user,rolable_type:"Teacher")
      PygmentsWorker.stub(:perform_async).and_return(true)
      sign_in_as(user,{:no_capybara => true})
      response.code.should == "200"
      response.should render_template('homes/show')
    end

    it "returns the posts in a reverse order for a Student" do
      get :show
      PygmentsWorker.stub(:perform_async).and_return(true)
      user = create(:user,rolable_type:"Student")
      sign_in_as(user,{:no_capybara => true})
      group = create(:group_with_notes_and_alerts)
      response.code.should == "200"
      response.should render_template('homes/show')
    end
  end
end
