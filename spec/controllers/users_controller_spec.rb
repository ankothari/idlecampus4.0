require 'spec_helper' 

describe UsersController do
  context "#create" do
    let(:user) { build(:user) }
    it "saves the teacher and redirects to the home path" do
      RubyBOSH.stub(:register).and_return(true)
      RubyBOSH.stub(:initialize_session).and_return([1,1,1])
      expect{
        post :create,user: {name: user.name, email: user.email, password: user.password}
      }.to change(User, :count).by(1)

      response.code.should == "302"
      response.should redirect_to(home_path)
    end

    it "does not save the student if group not found and redirects to the students signup page" do
      group = create(:group)
      RubyBOSH.stub(:register).and_return(true)
      RubyBOSH.stub(:initialize_session).and_return([1,1,1])
      expect{
        post :create,user: {name: user.name, email: user.email, password: user.password}, "Group Code" => "asdasd"
      }.to change(User, :count).by(0)

      response.code.should == "200"
      response.should render_template('new')
    end

    it "does save the student if group is found, makes the student follow the group and then redirects to the home path" do
      group = create(:group)
      RubyBOSH.stub(:register).and_return(true)
      RubyBOSH.stub(:initialize_session).and_return([1,1,1])
      expect{
        post :create,user: {name: user.name, email: user.email, password: user.password}, "Group Code" => group.group_code
      }.to change(User, :count).by(1)
      group.reload
      group.followers.count.should  eq(1)
      response.code.should == "302"
      response.should redirect_to(home_path)
    end
  end

  context "#update" do
    let(:original_user) { create(:user) }
    it "changes the device_identifier to the one sent" do
      patch :update,id: original_user.jabber_id,user: { device_identifier: "sadasnkdjnasdjnsajdabehewiuh" }
      original_user.reload
      expect(original_user.device_identifier).to eq("sadasnkdjnasdjnsajdabehewiuh")
      response.code.should == "200"
      response.body.should == "Device Changed Successfully"
    end  

    it "changes the device_identifier from of all the rest of users to not nil intact" do
      user1 = create(:user, device_identifier: original_user.device_identifier)
      patch :update,id: original_user.jabber_id,user: { device_identifier: "sadasnkdjnasdjnsajdabehewiuh" }
      user1.reload
      original_user.reload
      expect(user1.device_identifier).to eq("dskjfnskjfnsjkdn")
    end  

    it "changes the device_identifier for of all the users with the same device to nil" do
      user1 = create(:user, device_identifier: "sadasnkdjnasdjnsajdabehewiuh")
      patch :update,id: original_user.jabber_id,user: { device_identifier: "sadasnkdjnasdjnsajdabehewiuh" }
      user1.reload
      original_user
      expect(user1.device_identifier).to eq("")
    end  

    it "update method works even with the id" do
      patch :update,id: original_user.id, user: { device_identifier: "sadasnkdjnasdjnsajdabehewiuh" }
      original_user.reload
      expect(original_user.device_identifier).to eq("sadasnkdjnasdjnsajdabehewiuh")
      response.code.should == "200"
      response.body.should == "Device Changed Successfully"
    end  
  end
end


# {"email"=>"cool@ccol.com", "jabber_id"=>"cool@idlecampus.com", "user"=>{"device_identifier"=>"gfg"}, "id"=>"cool@idlecampus.com"}
