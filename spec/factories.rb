FactoryGirl.define do
  factory :user do

    sequence(:name) { |n| "#{Faker::Name::first_name}#{n}" }
    sequence(:email){ |n| "#{n}#{Faker::Internet.email}"}
    sequence(:jabber_id) { |n| "#{Faker::Name::first_name}#{n}@idlecampus.com"}
    device_identifier "dskjfnskjfnsjkdn"
    password "akk322" 
    password_confirmation "akk322"	
    rolable_type "Teacher"
    end
  end

FactoryGirl.define do 
  factory :room do
    sequence(:name) {|n| "room#{n}"}
  end
end

FactoryGirl.define do 
  factory :teacher do
    sequence(:name) {|n| "teacher#{n}"}
  end
end

FactoryGirl.define do 
  factory :subject do
    sequence(:name) {|n| "subject#{n}"}
  end
end

FactoryGirl.define do
  factory :note do
    message "This is a new note."
    sequence(:created_at) { |n|  Time.now + n}
    group
  end
end

FactoryGirl.define do
  factory :alert do
    message "This is a new alert."
    sequence(:created_at) { |n|  Time.now + n}
    group
  end
end



FactoryGirl.define do
	factory :group do
	  name "Electronics"
    group_code "9A2KVG"
    user
    factory :group_with_notes_and_alerts do
       after(:create) do |group, evaluator|
        notes = create_list(:note, 3)
        group.notes << notes
         alerts = create_list(:alert, 3)
        group.alerts << alerts
      end
    end
    factory :group_with_rooms do
     after(:create) do |group, evaluator|
        rooms = create_list(:room, 3)
        teachers = create_list(:teacher, 3)
        subjects = create_list(:subject, 3)
        group.rooms << rooms
        group.subjects << subjects
        group.teachers << teachers
      end
	  end
	end
end

FactoryGirl.define do
  factory :timetable do
    group
    factory :with_timetable_entries do
     after(:create) do |timetable, evaluator|
        create_list(:timetable_entry, 3,timetable: timetable)
      end
    end
  end
end





FactoryGirl.define do
  factory :timetable_entry do
    timetable
    weekday
    teacher
    subject
    room
    class_timing
    small_group
  end
end




FactoryGirl.define do
  factory :class_timing do
    sequence(:from) do |n|
      from = Time.new.utc
      from = from.change({:hour => n%24 , :min => n%60 })
    end
    sequence(:to) do |n|
       to = Time.new.utc
       to = to.change({:hour => n%24 , :min => n%60 })
    end
    
  end



FactoryGirl.define do 
  factory :weekday do
    sequence(:name) {|n| "weekday#{n}"}
  end
end


FactoryGirl.define do 
  factory :small_group do
    sequence(:name) {|n| "batch#{n}"}
  end
end


end

