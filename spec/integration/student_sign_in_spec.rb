require_relative '../spec_helper'
describe "Student signs in" do
  let(:user) { FactoryGirl.build(:user) }
  before do
    group = Group.create(name:'Electronics')
    sign_up_with_student(group.group_code,user.name,user.email,"akk322")
    click_link "Sign out"
  end
  it "with valid information" do
    click_link "Login"
    sign_in_as(user)
    page.should have_content "Welcome back to IdleCampus!" 
    page.should have_link('Sign out',  href: signout_path) 
  end
end

