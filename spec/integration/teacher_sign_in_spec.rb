require_relative '../spec_helper'

describe "Teacher signs in" do
  let(:user) { build(:user) }
  before do
    sign_up_with(user.name,user.email,"akk322")
    click_link "Sign out"
    click_link "Login"
  end
  it "with valid information" do
    sign_in_as(user)
    page.should have_content "Hi, #{user.name}"
    page.should have_link('Sign out',    href: signout_path) 
    page.should_not have_link('Login', href: signin_path)
  end
end
