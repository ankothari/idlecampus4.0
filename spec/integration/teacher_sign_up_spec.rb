require 'spec_helper'
feature 'Teachers signs up' do
  let(:user) {build(:user)}
  scenario 'with valid email and password' do
    sign_up_with user.name,user.email, 'akk322'
    page.should have_css "h1",'valid_name'
    expect(page).to have_link('Sign out')   
  end

  scenario 'with invalid email' do
    sign_up_with 'valid_name1','invalid_email', 'password'
    expect(page).to have_css("h3",text:"Sign up for a free account")
  end

  scenario 'with blank password' do
    sign_up_with 'valid_name2','valid@example.com', ''
    expect(page).to have_css("h3",text:"Sign up for a free account")
  end
end


   