require_relative '../spec_helper'
describe "Teacher creates a group" , :js => true do
  
  before do
    @user = FactoryGirl.build(:user)
    @group = @user.groups.build(name:'Electronics')
    sign_up_with(@user.name,@user.email,"akk322")
  end

  it "with a valid name" do
   
   click_button "Add"
   fill_in "new_group",with:@group.name
   click_button "Create" 
   expect(page).to have_css("a",text: @group.name)
  end 

  it "visits the group and sees the appropriate links" do
   click_button "Add"
   fill_in "new_group",with:@group.name
   click_button "Create" 
   click_link @group.name
   expect(page).to have_link "Files"
   expect(page).to have_link "Timetable"
  end
end

def open_page
  save_and_open_page
end


