require 'spec_helper'

feature "Manage content",js: true do
	 before do
    @user = FactoryGirl.build(:user)
    @group = @user.groups.build(name:'Electronics')
    sign_up_with(@user.name,@user.email,"akk322")
    click_button "Add"
    fill_in "new_group",with:@group.name
    click_button "Create" 
    click_link @group.name 
  end

 scenario "Teacher can upload a new note" do
   PygmentsWorker.stub(:perform_async).and_return(true)
   fill_in "note_message",with: "This is a new note"
   attach_file("note_file","/Users/apple/Documents/workspace/idlecampus4.0/spec/integration/groups_spec.rb")
   click_button "Upload"
   expect(page).to have_content "Uploading..."
  end

 scenario "Teacher can send an alert" do
    click_link "Alert"
    fill_in "alert_message",with: "This is a new alert"
    click_button "Send"
    expect(page).to have_content "This is a new alert"
  end
end