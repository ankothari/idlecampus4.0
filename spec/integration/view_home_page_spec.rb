require_relative '../spec_helper'

describe "User visits the homepage" do
  it "should see the links." do
    visit root_path
  	expect(page).to have_title "IdleCampus"
    expect(page).to have_css "a",text:"Sign up as teacher"
    expect(page).to have_css "a",text:"Sign up as student"
    expect(page).to have_css "a",text:"Login"
    expect(page).to have_css "a",text:"Contact Us"
    expect(page).to have_css "a",text:"About Us"
    expect(page).to have_css "a",text:"Blog"
  end
end
