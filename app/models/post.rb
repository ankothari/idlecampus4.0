class Post < ActiveRecord::Base
  belongs_to :group
  belongs_to :targetable,polymorphic: true
end
