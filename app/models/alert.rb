#
class Alert < ActiveRecord::Base
  belongs_to :group
  has_one :post, as: :targetable
end
