#
class Timetable < ActiveRecord::Base
  attr_accessor :message, :members
  belongs_to :group
  has_many :timetable_entries, -> { includes [:class_timing, :weekday, :small_group] } , dependent: :destroy

  delegate :get_field_entries, :to => :group
  has_many :weekdays, through: :timetable_entries

  def push_params
    { 
      from: group.name,
      members: members,
      message:  message,
      app: 'timetable' 
    }
  end

  def create_it_using(params)
    result = TimetableParser.new(params).parse
    @members = result["members"]
    @message = "http://idlecampus.com/groups/#{result['group_code']}/timetable.json"
    build_timetable_entries(result['entries'])
  end

  def get_entries_array
    entries = get_entries
    if entries.length > 0
      entries_array = []
      entries_sorted_by_weekday_and_class_timings = entries.group_by do
        |entry| [entry.weekday, entry.class_timing]
      end
      entries_sorted_by_weekday_and_class_timings_each_entry_in_hash = entries_sorted_by_weekday_and_class_timings
      entries_sorted_by_weekday_and_class_timings_each_entry_in_hash = entries_sorted_by_weekday_and_class_timings.values.each do |el|
        el.map! do |e|
          e.to_hash
        end
      end
      entries_sorted_by_weekday_and_class_timings_each_entry_in_hash_sorted_by_class_timings = entries_sorted_by_weekday_and_class_timings_each_entry_in_hash.group_by do |entry|
        [entry[0]['to_hours'],
          entry[0]['to_minutes'],
          entry[0]['from_hours'],
          entry[0]['from_minutes']]
      end
      entries_sorted_by_weekday_and_class_timings_each_entry_in_hash_sorted_by_class_timings.values
    end
  end

  def get_entries
    entries = timetable_entries
    entries.sort do |a, b| 
      a.class_timing <=> b.class_timing
    end
  end

  def get(entry)
    from, to = ClassTiming.time(entry)
    class_timing = ClassTiming.find_or_create_by(from: from, to: to)
    small_group = SmallGroup.find_or_create_by(name: entry['batch'])
    weekday = Weekday.find_or_create_by(name: entry['weekday'])
    TimetableEntry.find_or_create_by(timetable_id: self.id,
                                     weekday_id: weekday.id,
                                     class_timing_id: class_timing.id,
                                     small_group_id: small_group.id)
  end

  def build_timetable_entries(entries)
    PygmentsWorker.perform_async(push_params)
    entries.each do |ent|
      ent.each do |en|
        en.each do |entry|
          timetableentry = get(entry)
          entry.each do |key, value|
            timetableentry.create_entry( key, value)
          end
        end
      end
    end
  end
end
