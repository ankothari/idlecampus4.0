require 'drb'
require 'yaml'
require 'ruby_bosh'
class User < ActiveRecord::Base
  attr_reader :sid, :rid, :jid,:attacher
  devise :database_authenticatable,:registerable,
        :omniauthable, :omniauth_providers => [:github,:bitbucket]
  before_create :set_jabber_id
  
  after_create :save_user
  delegate :auth_token,to: :identity
  has_many :identities
  has_many :groups
  has_many :alerts,through: :groups
  has_many :notes,through: :groups
  has_many :created_groups, class_name: 'Group'
  acts_as_follower

  after_initialize :set_password_confirmation

  



def self.from_omniauth(auth)
  where(auth.slice(:provider, :uid)).first_or_create do |user|
    user.provider = auth.provider
    user.uid = auth.uid
    user.name = auth.info.nickname
    user.email = auth.info.email
  end
end

  def linked_with_github?
     identities.find_by(provider:"github")
  end
   def linked_with_bitbucket?
    identities.find_by(provider:"bitbucket")
  end

 def as_json(options={})
    super(options.merge(:include => :identities))
  end

def update_with_password(params, *options)
  if encrypted_password.blank?
    update_attributes(params, *options)
  else
    super
  end
end

  def save_user(options = {})
    self.register
    self.connect(self.password)
    self.follow(options[:group]) if options[:group]
  end

  def timeline_posts
    # group = self.all_following.first 
    # posts = []
    # posts << group.alerts.includes(:group)
    # posts << group.notes.includes(:group)
    # posts.flatten!
    # posts = posts.sort_by(&:created_at).reverse
  end

  def posts
    (self.alerts + self.notes).flatten.sort_by(&:created_at).reverse
  end

  def set_password_confirmation
    self.password_confirmation = self.password
  end

  def self.find_user(id)
    User.find_by_jabber_id(id) || User.find(id)
  end

  def register
    RubyBOSH.register(self)
  end



  def connect(password)
    @jid, @sid, @rid = RubyBOSH.initialize_session(self.jabber_id, password, Gon.global.xmpp_url)
  end

  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.zone.now
    save!
    UserMailer.password_reset(self).deliver
  end

  def generate_token(column)
    loop do
      self[column] = SecureRandom.urlsafe_base64
      break unless User.exists?(column => self[column])
    end
  end

  def self.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def self.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  def self.members_without_trailing_(jabber_ids)
    return nil if jabber_ids.nil?
    members = jabber_ids
    members.map do |member|
      index = member.index('/')
      if !index.nil?
        member.slice(0..index - 1)
      else
        member
      end
    end
  end

  def self.get_devices(jabber_ids)
    members = User.members_without_trailing_(jabber_ids)
    if members
      users = User.where(jabber_id: members)
      users.map do |user|
        user.device_identifier
      end
    end
  end

  def set_jabber_id
    self.jabber_id = "#{name}@#{Gon.global.host}"
  end

  def to_hash
    attacher = {}
    attacher[:name] = name
    attacher[:password] = password
  end

  private

    def create_remember_token
      self.remember_token = User.encrypt(User.new_remember_token)
    end
end
