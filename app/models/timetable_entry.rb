#
class TimetableEntry < ActiveRecord::Base
  belongs_to :timetable
  validates :timetable_id, presence: true
  belongs_to :weekday
  belongs_to :subject
  belongs_to :teacher
  belongs_to :room
  belongs_to :class_timing
  belongs_to :small_group
  
  delegate :to_hours, :to_minutes, :from_minutes, :from_hours, to: :class_timing
  delegate :group,to: :timetable
  
  default_scope -> { includes(:room).includes(:teacher).includes(:subject) }
  
  def to_hash
    TimetableEntrySerializer.new(self).serializable_hash.as_json
  end

  def create_entry(key, value)
    if key != 'from_hours' && key != 'from_minutes' && key != 'to_minutes' && key != 'to_hours' && key != 'weekday' && key != '$$hashKey' && key != 'batch'
      teacher = Teacher.find_or_create_by(name: value, group: group) if key == 'teacher'
      subject = Subject.find_or_create_by(name: value, group: group) if key == 'subject'
      room = Room.find_or_create_by(name: value, group: group) if key == 'room'
      self.subject = subject if key == 'subject'
      self.teacher = teacher if key == 'teacher'
      self.room = room if key == 'room'
      self.save
    end
  end

end
