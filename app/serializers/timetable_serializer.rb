class TimetableSerializer < ActiveModel::Serializer
  attributes :group_code,:weekdays,:batches,:field_entries,:entries

 def group_code
   object.group.group_code
 end
 
 def entries
    object.get_entries_array || []
 end
 
 def batches
   batches = []
   
   object.timetable_entries.each do |entry|
     batches << entry.small_group.name
   end
   batches.uniq.compact
 end
 

 def weekdays
   weekdays = []
   
   object.timetable_entries.each do |entry|
     weekdays << entry.weekday
   end
  weekdays.sort!.uniq!
  names = []
  weekdays.each do |weekday|
    names << weekday.name
  end
  names
 end
 
 def field_entries
   object.get_field_entries
 end


end
