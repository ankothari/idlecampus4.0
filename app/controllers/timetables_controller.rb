class TimetablesController < ApplicationController
  respond_to :json,:html

  def new
    @timetable = group.build_timetable
  end

  def create
    group.timetable.delete if group.timetable
    timetable = Timetable.create(group_id: group.id)
    timetable.create_it_using(params)
    render status: 200, nothing: true
  end

  def show
    @timetable = Timetable.find_or_create_by(group_id: group.id)
    gon.calendar = true
    respond_with @timetable
  end

  private

  def group
    @group || Group.find_group(params[:group_id])
  end


  def timetable_params
    params.require(:timetable)
  end


end
