class ActivitiesController < ApplicationController
  respond_to :html, :js
  def wow
    @activity = params[:class].constantize.find(params["activity_id"])
    @user = @activity.user
    @profile_user = User.find(session[:profile_id]) rescue nil
    @activity.liked_by current_user
  end

  def unwow
  	@activity = params[:class].constantize.find(params["activity_id"])
  	@user = @activity.user
    @profile_user = User.find(session[:profile_id]) rescue nil
    @activity.unliked_by current_user
  end

  def view_more
    if current_user.mutual_friends.count > 0
      @posts = Post.from_friends_of(current_user)
      user_ids = ([current_user.id] + current_user.friend_ids + current_user.inverse_friend_ids).join(',')
      @posts = @posts.where("creator_id IN (#{user_ids})").where("user_id IN (#{user_ids})")
    else
      @posts = current_user.posts
    end
    @num = params[:num].to_i
    @user = User.find(params[:user_id].to_i)
  end
end