require 'nokogiri'
require 'open-uri'
require "uri"
class PostsController < ApplicationController
  include ActionDispatch::Routing::UrlFor
  include ActionView::Helpers::TextHelper
  include Rails.application.routes.url_helpers
	respond_to :js
  def create
    @user = User.find(params[:user_id])
    @post = @user.posts.build(post_params)
    valid = true
    uris = URI.extract(params["post"]["name"])
    if uris.length > 0
      url = uris[0]
      if url.include?(".gif") or url.include?(".png") or url.include?(".jpg") or url.include?(".jpeg")
        @post.image = @post.picture_from_url(url)
        title = "<br>#{ActionController::Base.helpers.link_to('source', url,class: "posta",:target => "_blank")}<br>"
        @post.name = ""
        @status = title
        valid = true
      elsif url.include?(".html") or url.include?(".com") or url.include?(".in")
        begin
          page = MetaInspector.new(url)
          # doc = Nokogiri::HTML(open(url))
          @post.name.gsub!(url, "")
          # title = "<br>#{ActionController::Base.helpers.link_to(doc.at_css('title').text, url,class: "posta")}<br>"
          title = "<br>#{ActionController::Base.helpers.link_to(page.title, url,class: "posta",:target => "_blank")}<br>"
          @post.name << title
          @post.name << "<br>"
          @post.name << page.host
          @post.name << "<br><br>"
          @post.name << truncate(page.description, length: 500, separator: ' ')
          if page.description.length == 0
            @post.name <<  truncate(page.meta['og:description'], length: 500, separator: ' ')
          end
          valid = true
        rescue
          @error = "Please post a Web page URL that you would like to share with your friends"
          @post.delete
          valid = false
        end
      else
        @error = "Please post a Web page URL that you would like to share with your friends"
        @post.delete
        valid = false
      end
      # @post.name << url.split('/')[2]
      # @post.name << "<br>"
      # if doc.xpath("//meta[@name='description']/@content")[0]
      #   @post.name << doc.xpath("//meta[@name='description']/@content")[0].value
      # else
      #   @post.name << ""
      # end
    end
    if valid
      @post.creator = current_user
      @post.save
    end
      if @user.mutual_friends.count > 0
        @posts = Post.from_friends_of(@user)
        user_ids = ([@user.id] + @user.friend_ids + @user.inverse_friend_ids).join(',')
        @posts = @posts.where("creator_id IN (#{user_ids})").where("user_id IN (#{user_ids})")
      else
        @posts = @user.posts
      end
  end

  def destroy
  end

def index 
    
    group_ids = current_user.groups.map(&:id)
    
    @posts = Post.where("group_id in (?)",group_ids).order("created_at desc").all
  end

  private

  def post_params
    params.require(:post).permit(:name,:image)
  end
end
