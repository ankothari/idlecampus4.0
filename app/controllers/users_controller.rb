class UsersController < ApplicationController
  # protect_from_forgery except: :login
  before_action :signed_in_user
  def new
    @user = User.new
  end 

  def create
    @user = user_from_params
    options = {group: group}
    if group_not_found
      failed_signup('Group Not Found')
    elsif @user.save_user(options)
      flash[:attacher] = attacher(@user,options)
      sign_in_with_redirect(@user)
    else
      failed_signup('User Validation Error')
    end
  end

  def update
    update_all_users_who_are_not_web_to_device_identifier_as_blank
    user = User.find_user(params[:id])
    user.update_attributes(user_update_params)
    render text: 'Device Changed Successfully'
  end

  def show
    @user = User.find(params[:id])
  end

  def send_push
    #  {"users"=>"zb@idlecampus.com", "message"=>"dsvdxv", "controller"=>"users", "action"=>"send_push"}
    members = [params['to']]
    args = { members: members, message: params['message'], app: 'message' }
    PygmentsWorker.perform_async(args)
    render text: 'OK'
  end

  private

  def group_not_found
    params['Group Code'] && group.nil?
  end

  def user_from_params
    user = User.new(user_params)
    user.password = params["password"]
    user.device_identifier = "web"
    user
  end

  def group
    @group ||= Group.find_by(group_code: params['Group Code'])
  end

  def update_all_users_who_are_not_web_to_device_identifier_as_blank
    unless params[:device_identifier].eql? 'web'
      User.where(device_identifier: params[:user][:device_identifier]).update_all(device_identifier: '')
    end
  end

  def failed_signup(message)
    flash[:error] = message
    render 'new'
  end

  def sign_in_with_redirect(user)
    sign_in @user
    flash[:success] = 'Welcome to IdleCampus!'
    redirect_to home_path
  end

  def attacher(user,options = {})
    attacher =  {
      jid: user.jid,
      id: user.sid,
      rid: user.rid
    }
    attacher[:group] = group.group_code if options[:group]
    attacher
  end

  def user_params
    params.require(:user).permit(:name, :email, :password, :rolable_type,:device_identifier)
  end

  def user_update_params
    params.require(:user).permit(:device_identifier)
  end

  def signed_in_user
    redirect_to root_path, notice: 'Please sign in.' unless signed_in?
  end
end
