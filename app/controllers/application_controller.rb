class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :update_sanitized_params, if: :devise_controller?
  before_filter :log_additional_data

   protected
      def log_additional_data
        request.env["exception_notifier.exception_data"] = {
          :user => current_user || "Unknown"
        }
      end

  def update_sanitized_params
    devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:name, :rolable_type,:email,:password)}
  end

   def after_sign_in_path_for(user)
    dashboard_path
  end
end