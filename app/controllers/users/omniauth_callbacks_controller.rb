class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def idlecampus
    oauth_data = request.env["omniauth.auth"]
    @user = User.find_or_create_for_doorkeeper_oauth(oauth_data)
    @user.update_doorkeeper_credentials(oauth_data)
    @user.save

    sign_in_and_redirect root_path
  end

 def github
   auth = request.env['omniauth.auth']
    @identity = Identity.find_with_omniauth(auth)
    
  if @identity.nil?
    # If no identity was found, create a brand new one here
    @identity = Identity.create_with_omniauth(auth)
  else
    @identity.update_attributes({auth_token: auth['credentials']['token']})
  end
  
  if signed_in?
    if @identity.user == current_user
      # User is signed in so they are trying to link an identity with their
      # account. But we found the identity and the user associated with it 
      # is the current user. So the identity is already associated with 
      # this user. So let's display an error message.
      redirect_to dashboard_path
    else
      # The identity is not associated with the current_user so lets 
      # associate the identity
      @identity.user = current_user
      @identity.save()
    redirect_to dashboard_path
    end
  else
    @user = User.from_omniauth(auth)
    @identity.user = @user
    @identity.save
    if @user.encrypted_password.blank?
      sign_in @user
      redirect_to edit_user_registration_path(@user)
    else
      sign_in_and_redirect @user
    end
   end
  end

  def bitbucket
    auth = request.env['omniauth.auth']
    @identity = Identity.find_with_omniauth(auth)
    
  if @identity.nil?
    # If no identity was found, create a brand new one here
    @identity = Identity.create_with_omniauth(auth)
  else
    @identity.update_attributes({auth_token: auth['credentials']['token']})
  end
  
  if signed_in?
    if @identity.user == current_user
      # User is signed in so they are trying to link an identity with their
      # account. But we found the identity and the user associated with it 
      # is the current user. So the identity is already associated with 
      # this user. So let's display an error message.
      redirect_to dashboard_path
    else
      # The identity is not associated with the current_user so lets 
      # associate the identity
      @identity.user = current_user
      @identity.save()
    redirect_to dashboard_path
    end
  else
    @user = User.from_omniauth(auth)
    @identity.user = @user
    @identity.save
    if @user.encrypted_password.blank?
      sign_in @user
      redirect_to edit_user_registration_path(@user)
    else
      sign_in_and_redirect @user
    end
   end
  end





end
