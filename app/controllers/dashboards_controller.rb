class DashboardsController < ApplicationController
  before_filter :authenticate_user!, only: [:show]

	def show
		if signed_in? 
      gon.attacher = flash[:attacher]
      @group = Group.new(user: current_user)
      if current_user.rolable_type == "Teacher"
        @posts = current_user.posts
      elsif current_user.rolable_type == "Student"
        @posts = current_user.timeline_posts
      end
      @dashboard = UserDashboard.new(current_user)
    if current_user.mutual_friends.count > 0
      @posts = Post.from_friends_of(current_user)
      user_ids = ([current_user.id] + current_user.friend_ids + current_user.inverse_friend_ids).join(',')
      @posts = @posts.where("creator_id IN (#{user_ids})").where("user_id IN (#{user_ids})")
    else
      @posts = current_user.posts
    end
    end
	end
end
