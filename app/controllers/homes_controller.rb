class HomesController < ApplicationController
  before_filter :check_logged_in
  def show
    if signed_in? 
      gon.attacher = flash[:attacher]
      @group = Group.new(user: current_user)
      if current_user.rolable_type == "Teacher"
        @posts = current_user.posts
      elsif current_user.rolable_type == "Student"
        @posts = current_user.timeline_posts
      end
    end
  end
end