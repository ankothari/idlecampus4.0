class SessionsController < Devise::SessionsController
  respond_to :html, :js

  def new
    super
    gon.n = ""
    puts Gon.global.xmpp_url
  end
  
  def create
    if session[:user_return_to]
      warden.authenticate!(auth_options)
      redirect_to session[:user_return_to]
    else
      self.resource = warden.authenticate!(auth_options)
      set_flash_message(:notice, :signed_in) if is_flashing_format?
      sign_in(resource_name, resource)
      sign_in_as(self.resource)
      flash[:attacher] = attacher(self.resource)
      cookies[:login] = true
      cookies[:directed_from] = "http://idlecampus.com" 
      yield resource if block_given?
      respond_with resource, location: after_sign_in_path_for(resource)
    end
   end

  def destroy
    sign_out
    redirect_to root_url
  end

  private

  def sign_in_as(user)
    user.connect(params[:user][:password])
  end

  def attacher(user)
    {
      jid: user.jid,
      id: user.sid,
      rid: user.rid
    }
  end


end
