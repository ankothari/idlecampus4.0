#
class TimetableParser
  def initialize(params)
    @params = params
  end

  def parse
    {
      "group_code" => @params['group_id'],
      "entries" => ActiveSupport::JSON.decode(@params['timetable']['entries']),
      "members" =>  @params['timetable']['members']
    }
  end
end
  