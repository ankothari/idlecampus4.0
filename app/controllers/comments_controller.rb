class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  def create
    @comment = Comment.build_from(comment_params)

    respond_to do |format|
      if @comment.save
        @comment.move_to_child_of(@parent) if @parent
        format.js {
          if @user.mutual_friends.count > 0
            @posts = Post.from_friends_of(@user)
          else
            @posts = @user.posts
          end
          @activity_feed_user = User.find(params[:activity_feed_user_id])
          @commentable = params[:commentable_type].constantize.find(params[:commentable_id])
          render 'create'
        }
      else
        format.js {
          flash[:alert] = 'Sorry, but we cannot accept your last comment.'
        }
      end
    end
  end

  def destroy
    @comment.destroy
    respond_to do |format|
      format.js { flash[:message] = 'Your comment was removed from the conversation.' }
    end
  end

  private

  def set_comment
    @comment = Comment.find(params[:id])
  end

  def comment_params
    required_params = params.permit(:parent_id)
    @parent = Comment.find(params[:parent_id].to_i) rescue nil
    @user = User.find(params[:user_id])
    params.permit(:comment, :user_id, :commentable_id, :commentable_type)
  end
end