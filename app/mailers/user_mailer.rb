class UserMailer < ActionMailer::Base
  default from: "ankothari@gmail.com"

  def password_reset(user)
    @user = user
    mail :to => user.email, :subject => "Password Reset"
  end
  
  def signup_confirmation(user)
    @user = user
    mail :from => user.email, :to => "ankothari@gmail.com", :subject => "New Account"
  end
end