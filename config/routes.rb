IdleCampus::Application.routes.draw do
  
  use_doorkeeper
#   devise_scope :user do

# # get 'student/sign_up' => 'registrations#new', :user => { :user_type => 'student' }
# # get 'teacher/sign_up' => 'registrations#new', :user => { :user_type => 'teacher' }
# # get "users/sign_up" =>  redirect("/student/sign_up")
#  get "users/sign_up" => 'registrations#new', :user => { :user_type => 'teacher' }
# end
  devise_for :users,:controllers => {:sessions => 'sessions',:registrations => "registrations",omniauth_callbacks: "users/omniauth_callbacks"}
  resources :posts,only: [:index]
  get "password_resets/new"
  get "push/create"
  resources :password_resets
  match "checkEmail" => "user_validations#checkEmail",via: 'get'
  match "checkName" => "user_validations#checkName",via: 'get'
  resource :dashboard,only: [:show]
  post "users/frommobile"
  post "users/login"
  post "users/send_push"

  resources :users,except: :index,  :constraints => { :id => /[^\/]+/ }
  resource :home, only: :show
  resources :groups do
    resource :timetable,only: [:create,:show]
    resources :notes
    resources :files,only: :index
  end


    namespace :api do
    namespace :v1 do
      resources :profiles
      resources :users
      get '/me' => "credentials#me"
      get '/fast' => 'fast#index'
    end
  end
  
  # resources :teachers
  # resources :students
  
  resource :push,only: :create
 
  resources :notes
  resources :alerts, only: [:new, :create]
  namespace :api do
    resources :users,only: :create
    resources :groups,only: :show
  end
  root  'homes#show'
 
 
  match '/about',to: "static_pages#about",via: :get
  resources :messages, only: [:new, :create]
  match 'contact' => 'contact#new', :via => :get
  match 'contact' => 'contact#create',:via => :post
  
  
  namespace :api do
    resources :users, only: [:create]
  end
  
  resources :password_resets
  
end
